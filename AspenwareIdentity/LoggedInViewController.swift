//
//  LoggedInViewController.swift
//  AspenwareIdentity

// AppAuth:
// https://github.com/openid/AppAuth-iOS

/*
 Tutorial I followed:
 https://developer.forgerock.com/docs/platform/how-tos/implementing-oauth-20-authorization-code-grant-protected-pkce-appauth-sdk-ios#simple-app-rp
 https://github.com/ForgeRock/exampleOAuth2Clients/blob/6.5/iOS-AppAuth/iOS-AppAuth-IDM/Source/ViewController.swift
*/

// How to allow non https identity
// https://stackoverflow.com/questions/32631184/the-resource-could-not-be-loaded-because-the-app-transport-security-policy-requi

import UIKit
import AppAuth
import SafariServices
import AuthenticationServices

class LoggedInViewController: UIViewController, SFSafariViewControllerDelegate {

    let storeUrl = "https://dev-paymentplan.aspenwarecommerce.net"
    
    let clientId = "mobile"
    let redirectionUriScheme = "net.openid.appauthdemo"
    let issuerUrl: String = "https://identity-qa.aspenware.net"
    var redirectionUri: String {
        return redirectionUriScheme + "://oauth2redirect"
    }
    // The key under which the authorization state will be saved in a keyed archive.
    let authStateKey = "authState"
    var authState: OIDAuthState?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadState()
        showState()
    }
    
    @IBAction func getUserInfo(_ sender: Any) {
        getUserInfo()
    }
    
    @IBAction func goToStore(_ sender: Any) {
        if(authState != nil){
            doStoreRequest()
        } else {
        let vc = SFSafariViewController(url: URL(string: storeUrl)!)
            vc.delegate = self
            present(vc, animated: true)
        }
    }
    
    @IBAction func signOut(_ sender: Any) {
        endSession()
    }
}

// MARK: ForgeRock + AppAuth Code
extension LoggedInViewController {
    
    // Retrieve OIDC Configuration via discovery url
    func discoverOIDServiceConfiguration(_ issuerUrl: String, completion: @escaping (OIDServiceConfiguration?, Error?) -> Void) {
        // Checking if the issuer's URL can be constructed.
        guard let issuer = URL(string: issuerUrl) else {
            print("Error creating issuer URL for: \(issuerUrl)")
            return
        }

        print("Retrieving configuration for: \(issuer.absoluteURL)")

        // Discovering endpoints with AppAuth's convenience method.
        OIDAuthorizationService.discoverConfiguration(forIssuer: issuer) {
            configuration, error in

            // Completing with the caller's callback.
            completion(configuration, error)
        }
    }
    
    // Provide OIDC configuration manually
    func getOIDCProviderConfiguration() -> OIDServiceConfiguration {
        let configuration = OIDServiceConfiguration.init(
            authorizationEndpoint: URL(string: issuerUrl + "/connect/authorize")!,
            tokenEndpoint: URL(string: issuerUrl + "/connect/token")!
        )

        return configuration
    }
    
    func doStoreRequest() {
        // Checking if the redirection URL can be constructed.
        guard let redirectURI = URL(string: redirectionUri) else {
            print("Error creating redirection URL for : \(redirectionUri)")

            return
        }

        // Checking if the AppDelegate property holding the authorization session could be accessed
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            print("Error accessing AppDelegate")

            return
        }
        
        
        let scopes: [String] = [OIDScopeOpenID, OIDScopeProfile, "offline_access"]
        
        let configuration = OIDServiceConfiguration.init(
            authorizationEndpoint: URL(string: storeUrl + "/login")!,
            tokenEndpoint: URL(string: storeUrl + "/login")!
        )

        //let userAgent = OIDExternalUserAgentIOSCustomBrowser.customBrowserSafari()
        // Building authorization request.
        let request = OIDAuthorizationRequest(
            configuration: configuration,
            clientId: clientId,
            clientSecret: nil,
            scopes: scopes,
            redirectURL: redirectURI,
            responseType: OIDResponseTypeCode,
            additionalParameters: nil
        )

        appDelegate.currentAuthorizationFlow = OIDAuthState.authState(byPresenting: request, presenting: self) {
            authState, error in
            // Do nothing
        }
    }
    
    func endSession (){
        // Checking if the redirection URL can be constructed.
        guard let postLogoutRedirectURI = URL(string: redirectionUri) else {
            print("Error creating redirection URL for : \(redirectionUri)")

            return
        }
        
        // Checking if the AppDelegate property holding the authorization session could be accessed
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            print("Error accessing AppDelegate")

            return
        }
        
        discoverOIDServiceConfiguration(issuerUrl) {
            configuration, error in
                guard let configuration = configuration else {
                    print("Error retrieving discovery document ")
                    self.setAuthState(nil)
                    return
                }
                
                if let idToken = self.authState?.lastTokenResponse?.idToken {
                    let request = OIDEndSessionRequest(
                        configuration:configuration,
                        idTokenHint: idToken,
                        postLogoutRedirectURL: postLogoutRedirectURI,
                        additionalParameters: nil)
                guard let agent = OIDExternalUserAgentIOS(presenting: self) else {
                    return
                }
                appDelegate.currentAuthorizationFlow = OIDAuthorizationService.present(request, externalUserAgent: agent,
                                                                                callback: { (response, error) in
                    if let response = response {
                        //delete cookies just in case
                        HTTPCookieStorage.shared.cookies?.forEach { cookie in
                            HTTPCookieStorage.shared.deleteCookie(cookie)
                        }
                        // successfully logout
                        
                        self.setAuthState(nil)
                        self.saveState()
                        self.navigationController?.popViewController(animated: true)
                        self.dismiss(animated: true, completion: nil)
                    }
                    if let err = error {
                       // print Error
                    }
                })
            }
        }
    }
    
    func saveState() {
        var data: Data? = nil

        if let authState = self.authState {
            if #available(iOS 12.0, *) {
                data = try! NSKeyedArchiver.archivedData(withRootObject: authState, requiringSecureCoding: false)
            } else {
                data = NSKeyedArchiver.archivedData(withRootObject: authState)
            }
        }

        UserDefaults.standard.set(data, forKey: authStateKey)
        UserDefaults.standard.synchronize()

        print("Authorization state has been saved.")
    }

    // Reacts on authorization state changes events.
    func stateChanged() {
        self.saveState()

        if authState != nil {
            //getUserInfo()
        }
    }

    // Assigns the passed in authorization state to the class property.
    // Assigns this controller to the state delegate property.
    func setAuthState(_ authState: OIDAuthState?) {
        if (self.authState != authState) {
            self.authState = authState

            self.authState?.stateChangeDelegate = self

            self.stateChanged()
        }
    }

    // Loads authorization state from a storage.
    // As an example, the user's defaults database serves as the persistent storage.
    func loadState() {
        guard let data = UserDefaults.standard.object(forKey: authStateKey) as? Data else {
            return
        }

        var authState: OIDAuthState? = nil

        if #available(iOS 12.0, *) {
            authState = try! NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? OIDAuthState
        } else {
            authState = NSKeyedUnarchiver.unarchiveObject(with: data) as? OIDAuthState
        }

        if let authState = authState {
            print("Authorization state has been loaded.")

            self.setAuthState(authState)
        }
    }
    
    func showState() {
        print("Current authorization state: ")

        print("Access token: \(authState?.lastTokenResponse?.accessToken ?? "none")")

        print("ID token: \(authState?.lastTokenResponse?.idToken ?? "none")")

        print("Expiration date: \(String(describing: authState?.lastTokenResponse?.accessTokenExpirationDate))")
        
        //let idTokenClaims = getIdTokenClaims(idToken: authState?.lastTokenResponse?.idToken ?? "") ?? Data()
        //print("ID token claims: \(String(describing: String(bytes: idTokenClaims, encoding: .utf8)))")
    }
    
    func getUserInfo() {
            guard let url = authState?.lastAuthorizationResponse.request.configuration.discoveryDocument?.userinfoEndpoint else {
                print("Userinfo endpoint not declared in discovery document.")

                return
            }

            let urlRequest = URLRequest(url: url)

            self.makeUrlRequestToProtectedResource(urlRequest: urlRequest){
                data, response, request in

                var text = "User Info:\n"

                text += "\nREQUEST:\n"
                text += "URL: " + (request.url?.absoluteString ?? "") + "\n"

                //text += "HEADERS: \n"
                //request.allHTTPHeaderFields?.forEach({
                //    header in

                //    text += "\"\(header.key)\": \"\(header.value)\"\n"
                //})

                print(request.description)
                text += "\nRESPONSE:\n"
                text += "Status Code: " + String(response.statusCode) + "\n"

                //text += "HEADERS:\n"
                //response.allHeaderFields.forEach({
                //    header in

                //    text += "\"\(header.key)\": \"\(header.value)\"\n"
                //})

                text += "\nDATA:\n"
                if let data = data {
                    text += String(bytes: data, encoding: .utf8)!
                }

                print(text)
            }
        }
    }
    // MARK: URL request helpers
    extension LoggedInViewController {
        /**
        Sends a URL request.

        Sends a predefined request and handles common errors.

        - Parameter urlRequest: URLRequest optionally crafted with additional information, which may include access token.
        - Parameter completion: Escaping completion handler allowing the caller to process the response.
        */
        func sendUrlRequest(urlRequest: URLRequest, completion: @escaping (Data?, HTTPURLResponse, URLRequest) -> Void) {
            let task = URLSession.shared.dataTask(with: urlRequest) {
                data, response, error in

                DispatchQueue.main.async {
                    guard error == nil else {
                        // Handling transport error
                        print("HTTP request failed \(error?.localizedDescription ?? "")")

                        return
                    }

                    guard let response = response as? HTTPURLResponse else {
                        // Expecting HTTP response
                        print("Non-HTTP response")

                        return
                    }

                    completion(data, response, urlRequest)
                }
            }

            task.resume()
        }
        
        /**
        Makes a request to a protected source that accepts tokens from the OIDC Provider.

        - Parameter urlRequest: URLRequest with pre-defined URL, method, etc.
        - Parameter completion: Escaping completion handler allowing the caller to process the response.
        */
        func makeUrlRequestToProtectedResource(urlRequest: URLRequest, completion: @escaping (Data?, HTTPURLResponse, URLRequest) -> Void) {
            let currentAccessToken: String? = self.authState?.lastTokenResponse?.accessToken

            // Validating and refreshing tokens
            self.authState?.performAction() {
                accessToken, idToken, error in

                if error != nil {
                    print("Error fetching fresh tokens: \(error?.localizedDescription ?? "")")


                    return
                }

                guard let accessToken = accessToken else {
                    print("Error getting accessToken")

                    return
                }

                if currentAccessToken != accessToken {
                    print("Access token was refreshed automatically (\(currentAccessToken ?? "none") to \(accessToken))")
                } else {
                    print("Access token was fresh and not updated \(accessToken)")
                }

                var urlRequest = urlRequest

                // Including the access token in the request
                var requestHeaders = urlRequest.allHTTPHeaderFields ?? [:]
                requestHeaders["Authorization"] = "Bearer \(accessToken)"
                urlRequest.allHTTPHeaderFields = requestHeaders

                self.sendUrlRequest(urlRequest: urlRequest) {
                    data, response, request in

                    guard let data = data, data.count > 0 else {
                        print("HTTP response data is empty.")

                        return
                    }

                    if response.statusCode != 200 {
                        // Server replied with an error
                        let responseText: String? = String(data: data, encoding: String.Encoding.utf8)

                        if response.statusCode == 401 {
                            // "401 Unauthorized" generally indicates there is an issue with the authorization grant; hence, putting OIDAuthState into an error state.

                            // Checking if the response is a valid JSON.
                            var json: [AnyHashable: Any]?

                            do {
                                json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                            } catch {
                                print("JSON Serialization Error.")
                            }

                            let oauthError = OIDErrorUtilities.resourceServerAuthorizationError(
                                withCode: 0,
                                errorResponse: json,
                                underlyingError: nil
                            )

                            self.authState?.update(withAuthorizationError: oauthError)

                            print("Authorization Error (\(oauthError)). Response: \(responseText ?? "")")
                        } else {
                            print("HTTP: \(response.statusCode), Response: \(responseText ?? "")")
                        }
                    }

                    completion(data, response, urlRequest)
                }
            }
        }
    }


// MARK: OIDAuthState delegates
extension LoggedInViewController: OIDAuthStateChangeDelegate {
    // Responds to authorization state changes in the AppAuth library.
    func didChange(_ state: OIDAuthState) {
        print("Authorization state change event.")

        self.stateChanged()
    }
}

extension LoggedInViewController: OIDAuthStateErrorDelegate {
    // Reports authorization errors in the AppAuth library.
    func authState(_ state: OIDAuthState, didEncounterAuthorizationError error: Error) {
        print("Received authorization error: \(error)")
    }
}

extension LoggedInViewController: ASWebAuthenticationPresentationContextProviding {
    func presentationAnchor(for session: ASWebAuthenticationSession) -> ASPresentationAnchor {
        return view.window!
    }
}



