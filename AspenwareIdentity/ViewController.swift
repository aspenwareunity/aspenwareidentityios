//
//  ViewController.swift
//  AspenwareIdentity


// AppAuth:
// https://github.com/openid/AppAuth-iOS

/*
 Tutorial I followed:
 https://developer.forgerock.com/docs/platform/how-tos/implementing-oauth-20-authorization-code-grant-protected-pkce-appauth-sdk-ios#simple-app-rp
 https://github.com/ForgeRock/exampleOAuth2Clients/blob/6.5/iOS-AppAuth/iOS-AppAuth-IDM/Source/ViewController.swift
*/

// How to allow non https identity
// https://stackoverflow.com/questions/32631184/the-resource-could-not-be-loaded-because-the-app-transport-security-policy-requi

import UIKit
import AppAuth
import SafariServices
import AuthenticationServices

class ViewController: UIViewController, SFSafariViewControllerDelegate {

    let storeUrl = "https://dev-paymentplan.aspenwarecommerce.net"
    
    let clientId = "mobile"
    let redirectionUriScheme = "net.openid.appauthdemo"
    let issuerUrl: String = "https://identity-qa.aspenware.net"
    var redirectionUri: String {
        return redirectionUriScheme + "://oauth2redirect"
    }
    // The key under which the authorization state will be saved in a keyed archive.
    let authStateKey = "authState"
    var authState: OIDAuthState?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        refreshState()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if(self.authState != nil){
            let mainStoryBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:Bundle.main)
            guard let loggedInViewController = mainStoryBoard.instantiateViewController(withIdentifier: "LoggedInViewController") as? LoggedInViewController else {
                print("Couldn't find LoggedInViewController")
                return
            }
            
            self.show(loggedInViewController, sender: self)
        }
    }
    
    @IBAction func goToStore(_ sender: Any) {
        refreshState()
        if(authState != nil){
            doStoreRequest()
        } else {
        let vc = SFSafariViewController(url: URL(string: storeUrl)!)
            vc.delegate = self
            present(vc, animated: true)
        }
        
    }
    
    @IBAction func signIn(_ sender: Any) {
        authorizeRP(issuerUrl: issuerUrl, configuration: nil)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
}

// MARK: ForgeRock + AppAuth Code
extension ViewController {
    
    func refreshState(){
        loadState()
        showState()
    }
    // Retrieve OIDC Configuration via discovery url
    func discoverOIDServiceConfiguration(_ issuerUrl: String, completion: @escaping (OIDServiceConfiguration?, Error?) -> Void) {
        // Checking if the issuer's URL can be constructed.
        guard let issuer = URL(string: issuerUrl) else {
            print("Error creating issuer URL for: \(issuerUrl)")
            return
        }

        print("Retrieving configuration for: \(issuer.absoluteURL)")

        // Discovering endpoints with AppAuth's convenience method.
        OIDAuthorizationService.discoverConfiguration(forIssuer: issuer) {
            configuration, error in

            // Completing with the caller's callback.
            completion(configuration, error)
        }
    }
    
    // Provide OIDC configuration manually
    func getOIDCProviderConfiguration() -> OIDServiceConfiguration {
        let configuration = OIDServiceConfiguration.init(
            authorizationEndpoint: URL(string: issuerUrl + "/connect/authorize")!,
            tokenEndpoint: URL(string: issuerUrl + "/connect/token")!
        )

        return configuration
    }
    
    func doStoreRequest() {
        // Checking if the redirection URL can be constructed.
        guard let redirectURI = URL(string: redirectionUri) else {
            print("Error creating redirection URL for : \(redirectionUri)")

            return
        }

        // Checking if the AppDelegate property holding the authorization session could be accessed
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            print("Error accessing AppDelegate")

            return
        }
        
        
        let scopes: [String] = [OIDScopeOpenID, OIDScopeProfile, "offline_access"]
        
        let configuration = OIDServiceConfiguration.init(
            authorizationEndpoint: URL(string: storeUrl + "/login")!,
            tokenEndpoint: URL(string: storeUrl + "/login")!
        )

        //let userAgent = OIDExternalUserAgentIOSCustomBrowser.customBrowserSafari()
        // Building authorization request.
        let request = OIDAuthorizationRequest(
            configuration: configuration,
            clientId: clientId,
            clientSecret: nil,
            scopes: scopes,
            redirectURL: redirectURI,
            responseType: OIDResponseTypeCode,
            additionalParameters: nil
        )

        appDelegate.currentAuthorizationFlow = OIDAuthState.authState(byPresenting: request, presenting: self) {
            authState, error in
            // Do nothing
        }
    }
    
    // Include offline_access scope for refresh token
    func authorizeWithAutoCodeExchange(
        configuration: OIDServiceConfiguration,
        clientId: String,
        redirectionUri: String,
        scopes: [String] = [OIDScopeOpenID, OIDScopeProfile, "offline_access"],
        completion: @escaping (OIDAuthState?, Error?) -> Void
        ) {
        // Checking if the redirection URL can be constructed.
        guard let redirectURI = URL(string: redirectionUri) else {
            print("Error creating redirection URL for : \(redirectionUri)")

            return
        }

        // Checking if the AppDelegate property holding the authorization session could be accessed
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            print("Error accessing AppDelegate")

            return
        }

        //let userAgent = OIDExternalUserAgentIOSCustomBrowser.customBrowserSafari()
        // Building authorization request.
        let request = OIDAuthorizationRequest(
            configuration: configuration,
            clientId: clientId,
            clientSecret: nil,
            scopes: scopes,
            redirectURL: redirectURI,
            responseType: OIDResponseTypeCode,
            additionalParameters: nil
        )

        // Making authorization request.

        print("Initiating authorization request with scopes: \(request.scope ?? "no scope requested")")

        //appDelegate.currentAuthorizationFlow = OIDAuthState.authState(byPresenting: request, externalUserAgent: userAgent) {
        appDelegate.currentAuthorizationFlow = OIDAuthState.authState(byPresenting: request, presenting: self) {
            authState, error in

            completion(authState, error)
        }
    }
    
    
    func authorizeRP(issuerUrl: String?, configuration: OIDServiceConfiguration?, completion: (() -> Void)? = nil) {

        func authorize(configuration: OIDServiceConfiguration) {
            print("Authorizing with configuration: \(configuration)")
            
            self.authorizeWithAutoCodeExchange(
                configuration: configuration,
                clientId: self.clientId,
                redirectionUri: self.redirectionUri
            ) {
                authState, error in
                
                if let authState = authState {
                    
                    self.setAuthState(authState)
                    
                    print("Successful authorization.")
                    
                    self.showState()
                    
                    if let completion = completion {
                        completion()
                    }
                    
                    let mainStoryBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:Bundle.main)
                    
                    
                    guard let loggedInViewController = mainStoryBoard.instantiateViewController(withIdentifier: "LoggedInViewController") as? LoggedInViewController else {
                        print("Couldn't find LoggedInViewController")
                        return
                    }
                    
                    self.show(loggedInViewController, sender: self)
                } else {
                    print("Authorization error: \(error?.localizedDescription ?? "")")
                    
                    self.setAuthState(nil)
                }
            }
        }

        // Use this if dicovery page is available
        if let issuerUrl = issuerUrl {
            // Discovering OP configuration
            discoverOIDServiceConfiguration(issuerUrl) {
                configuration, error in

                guard let configuration = configuration else {
                    print("Error retrieving discovery document for \(issuerUrl): \(error?.localizedDescription ?? "")")
                    //self.setAuthState(nil)
                    return
                }
                authorize(configuration: configuration)
            }
        }
        // Use this for manual config
        else if let configuration = configuration {
            // Accepting passed-in OP configuration
            authorize(configuration: configuration)
        }
    }
    
    func saveState() {
        var data: Data? = nil

        if let authState = self.authState {
            if #available(iOS 12.0, *) {
                data = try! NSKeyedArchiver.archivedData(withRootObject: authState, requiringSecureCoding: false)
            } else {
                data = NSKeyedArchiver.archivedData(withRootObject: authState)
            }
        }

        UserDefaults.standard.set(data, forKey: authStateKey)
        UserDefaults.standard.synchronize()

        print("Authorization state has been saved.")
    }

    // Reacts on authorization state changes events.
    func stateChanged() {
        self.saveState()

        if authState != nil {
            
        }
    }

    // Assigns the passed in authorization state to the class property.
    // Assigns this controller to the state delegate property.
    func setAuthState(_ authState: OIDAuthState?) {
        if (self.authState != authState) {
            self.authState = authState

            self.authState?.stateChangeDelegate = self

            self.stateChanged()
        }
    }

    // Loads authorization state from a storage.
    // As an example, the user's defaults database serves as the persistent storage.
    func loadState() {
        guard let data = UserDefaults.standard.object(forKey: authStateKey) as? Data else {
            self.setAuthState(nil)
            self.saveState()
            return
        }

        var authState: OIDAuthState? = nil

        if #available(iOS 12.0, *) {
            authState = try! NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? OIDAuthState
        } else {
            authState = NSKeyedUnarchiver.unarchiveObject(with: data) as? OIDAuthState
        }

        if let authState = authState {
            print("Authorization state has been loaded.")

            self.setAuthState(authState)
        }
    }
    
    func showState() {
        print("Current authorization state: ")

        print("Access token: \(authState?.lastTokenResponse?.accessToken ?? "none")")

        print("ID token: \(authState?.lastTokenResponse?.idToken ?? "none")")

        print("Expiration date: \(String(describing: authState?.lastTokenResponse?.accessTokenExpirationDate))")
        
        //let idTokenClaims = getIdTokenClaims(idToken: authState?.lastTokenResponse?.idToken ?? "") ?? Data()
        //print("ID token claims: \(String(describing: String(bytes: idTokenClaims, encoding: .utf8)))")
    }
    
}


// MARK: OIDAuthState delegates
extension ViewController: OIDAuthStateChangeDelegate {
    // Responds to authorization state changes in the AppAuth library.
    func didChange(_ state: OIDAuthState) {
        print("Authorization state change event.")

        self.stateChanged()
    }
}

extension ViewController: OIDAuthStateErrorDelegate {
    // Reports authorization errors in the AppAuth library.
    func authState(_ state: OIDAuthState, didEncounterAuthorizationError error: Error) {
        print("Received authorization error: \(error)")
    }
}

extension ViewController: ASWebAuthenticationPresentationContextProviding {
    func presentationAnchor(for session: ASWebAuthenticationSession) -> ASPresentationAnchor {
        return view.window!
    }
}


